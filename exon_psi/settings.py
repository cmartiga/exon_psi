#!/usr/bin/env python
from os.path import join, abspath, dirname

BASE_DIR = abspath(join(dirname(__file__), '..'))
BIN_DIR = join(BASE_DIR, 'bin')
TEST_DATA_DIR = join(BASE_DIR, 'test', 'test_data')

# Programs labels
MAJIQ = 'majiq'
RMATS = 'rmats'
WHIPPET = 'whipppet'
SPLICETRAP = 'splicetrap'
SUPPA = 'suppa'
VASTTOOLS = 'vasttools'
SPLADDER = 'spladder'
OUTRIGGER = 'outrigger'
# FREEPSI = 'freepsi'

AVAILABLE_PROGRAMS = [MAJIQ, RMATS, WHIPPET, SPLICETRAP, SUPPA, SPLADDER,
                      OUTRIGGER, VASTTOOLS]
PROG_EVENTS_REQ = [RMATS]