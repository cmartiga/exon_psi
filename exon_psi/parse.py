#!/usr/bin/env python
from os.path import join

import pandas as pd
import numpy as np

from exon_psi.functions import calc_majiq_exon_psi
from exon_psi.settings import (MAJIQ, AVAILABLE_PROGRAMS, RMATS, WHIPPET,
                               SPLICETRAP, SUPPA, SPLADDER, OUTRIGGER,
                               VASTTOOLS)


def to_0_indexed_exon_id(coord, strand=False):
    if strand:
        strand = coord[-1]
        coord = coord[:-1]
    chrom, cs = coord.split(':')
    start, end = cs.split('-')
    start = int(start) - 1
    exon_id = '{}:{}-{}'.format(chrom, start, end)
    if strand:
        exon_id = exon_id + strand
    return(exon_id)


class PSIparser():
    def __init__(self, program):
        self.parse = self._get_program_parser(program)
    
    @property
    def parsers(self):
        parsers = {MAJIQ: self.parse_majiq_psi,
                   RMATS: self.parse_rmats_psi, 
                   WHIPPET: self.parse_whippet_psi,
                   SPLICETRAP: self.parse_splicetrap_psi,
                   SUPPA: self.parse_suppa_psi,
                   VASTTOOLS: self.parse_vasttools_psi,
                   SPLADDER: self.parse_spladder_psi,
                   OUTRIGGER: self.parse_outrigger_psi}
        return(parsers)
    
    def _get_program_parser(self, program):
        parsers = self.parsers
        if program not in parsers:
            msg = 'Program {} not supported yet. Did you mean any of these? {}'
            raise ValueError(msg.format(AVAILABLE_PROGRAMS))
        return(parsers[program])
    
    def collapse_overlapping_exon_skipping(self, data):
        total = data.groupby('exon_id')['total'].sum().to_dict()
        data['total_exon'] = [total[e] for e in data['exon_id']]
        data['psi'] = data['total'] / data['total_exon'] * data['psi']
        data = data.groupby('exon_id')[['psi', 'total']].sum().reset_index()
        return(data)
        
    def parse_majiq_psi(self, fpath, only_simple_events, **kwargs):
        data = pd.read_csv(fpath, sep='\t')
        psi = calc_majiq_exon_psi(data, only_simple_events=only_simple_events)
        return(psi)
    
    def get_rmats_matrix(self, v):
        m = pd.DataFrame([[float(x) if x != 'NA' else np.nan
                           for x in row.split(',')] for row in v],
                         index=v.index)
        return(m)
    
    def collapse_overlapping_exon_skipping_matrices(self, data, psi, total):
        records = []
        for exon_id, exon_data in data.groupby('exon_id'):
            event_ids = exon_data.index
            event_total = total.loc[event_ids, :]
            exon_total = event_total.sum(0) 
            exon_total_p = event_total / exon_total
            exon_p = np.sum(psi.loc[event_ids] * exon_total_p, axis=0)
            
            record = {'exon_id': exon_id}
            record.update({'psi.{}'.format(i): p for i, p in enumerate(exon_p)})
            record.update({'total.{}'.format(i): p for i, p in enumerate(exon_total.astype(int))})
            records.append(record)
        data = pd.DataFrame(records) 
        return(data)
    
    def parse_rmats_psi(self, rmats_dir, use_exon_counts=False, **kwargs):
        # Load rMATS events data
        if use_exon_counts:
            fpath = join(rmats_dir, 'SE.MATS.JCEC.txt')
        else:
            fpath = join(rmats_dir, 'SE.MATS.JC.txt')
        data = pd.read_csv(fpath, dtype={'ID': 'str'}, sep='\t').set_index('ID')
        exon_data = zip(data['chr'], data['exonStart_0base'],
                   data['exonEnd'], data['strand'])
        data['exon_id'] = ['{}:{}-{}{}'.format(chrom, start, end, strand)
                           for chrom, start, end, strand in exon_data]
        
        # Load data data
        inclusion = self.get_rmats_matrix(data['IJC_SAMPLE_1'])
        skipping = self.get_rmats_matrix(data['SJC_SAMPLE_1'])
        total = inclusion + skipping
        psi = self.get_rmats_matrix(data['IncLevel1'])
        
        # Calculate event weight for averaging at the exon level
        data = self.collapse_overlapping_exon_skipping_matrices(data, psi, total)
        return(data)
    
    def parse_whippet_psi(self, fpath, **kwargs):
        psi = pd.read_csv(fpath, sep='\t', compression='gzip')
        psi['exon_id'] = ['{}{}'.format(to_0_indexed_exon_id(coord), strand)
                          for coord, strand in zip(psi['Coord'], psi['Strand'])]
        columns = ['exon_id', 'Psi', 'Total_Reads']
        psi = psi[columns]
        psi.columns = ['exon_id', 'psi', 'total']
        psi['total'] = psi['total'].replace(np.nan, 0).astype(int)
        return(psi)

    def _extract_vasttools_total(self, str_value):
        counts = str_value.split('@')[-1].split(',')
        total = sum([float(x) for x in counts])
        return(int(total))
    
    def get_vasttools_total_counts(self, data):
        total = [self._extract_vasttools_total(v) for v in data.iloc[:, 7]]
        return(total)
    
    def parse_vasttools_psi(self, fpath, **kwargs):
        '''
        As no strand information is provided, we return the PSI for the exon
        in both strands
        '''
        data = pd.read_csv(fpath, sep='\t').dropna()
        exons = [x[3:5] == 'EX' for x in data['EVENT']]
        data = data.loc[exons, :]
        total = self.get_vasttools_total_counts(data)
        psi = data.iloc[:, 6].astype(float)
        d1 = pd.DataFrame({'exon_id': [to_0_indexed_exon_id(x) + '+'
                                       for x in data['COORD']],
                           'psi': psi, 'total': total})
        d2 = pd.DataFrame({'exon_id': [to_0_indexed_exon_id(x) + '-'
                                       for x in data['COORD']],
                           'psi': psi, 'total': total})
        psi = pd.concat([d1, d2])
        psi['psi'] = psi['psi'] / 100
        return(psi)
    
    def parse_splicetrap_psi(self, fpath, **kwargs):
        data = pd.read_csv(fpath, sep='\t', header=None)
        events_data = zip(data[2], data[4], data[6])
        data['exon_id'] = ['{}:{}{}'.format(chrom, coords.replace(',', '-'), strand)
                           for chrom, coords, strand in events_data]
        data['total'] = data[7] + data[8] + data[9]
        data['psi'] = data[1]
        psi = self.collapse_overlapping_exon_skipping(data)
        return(psi)
    
    def parse_suppa_psi(self, fpath, **kwargs):
        data = []
        with open(fpath) as fhand:
            for line in fhand:
                items = line.strip().split()
                if len(items) < 2:
                    continue
                exon_id, psi = items
                id_items = exon_id.split(';')[-1].split(':')
                _, chrom, donor, acceptor, strand = id_items
                start, end = donor.split('-')[1], acceptor.split('-')[0]
                start = int(start) - 1
                exon_id = '{}:{}-{}{}'.format(chrom, start, end, strand)
                data.append({'exon_id': exon_id, 'psi': psi})
        psi = pd.DataFrame(data)
        return(psi)
    
    def parse_spladder_psi(self, fpath, **kwargs):
        data = pd.read_csv(fpath, sep='\t')
        field_psi = data.columns[-1]
        counts_fields = ['priming_fragment.sorted:exon_pre_cov',
                         'priming_fragment.sorted:exon_cov',
                         'priming_fragment.sorted:exon_aft_cov']
        total = data[counts_fields].sum(1)
        exon_data = zip(data['contig'], data['exon_start'],
                        data['exon_end'], data['strand'])
        exon_ids = ['{}:{}-{}{}'.format(chrom, int(start)-1, end, strand)
                    for chrom, start, end, strand in exon_data]
        psi = pd.DataFrame({'exon_id': exon_ids, 'psi': data[field_psi],
                            'total': total})
        psi = self.collapse_overlapping_exon_skipping(psi)
        return(psi)
    
    def parse_outrigger_psi(self, fpath, **kwargs):
        data = []
        with open(fpath) as fhand:
            for line in fhand:
                items = line.strip().split(',')
                if items[0] == 'event_id':
                    continue
                if len(items) < 2:
                    continue
                event_id, psi = items
                _, chrom, coords, strand = event_id.split('@')[1].split(':')
                start, end = coords.split('-')
                start = int(start) - 1
                exon_id = '{}:{}-{}{}'.format(chrom, start, end, strand)
                data.append({'exon_id': exon_id, 'psi': psi})
        psi = pd.DataFrame(data)
        return(psi)
        