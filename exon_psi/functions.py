#!/usr/bin/env python
import pandas as pd
import numpy as np
from _collections import defaultdict
from _functools import partial
from itertools import combinations


def get_coords(coords_str):
    return(tuple([int(x) if x != 'nan' else np.inf
                  for x in coords_str.split('-')]))
    

def calc_distance(exon, pos):
    start, end = exon
    if pos == start or pos == end:
        d = -1
    elif pos >= start and pos <= end:
        d = 0
    else:
        d = min(abs(pos - start), abs(pos - end))
    return(d)


def get_closest_exon(pos, exons):
    return(tuple(min(exons, key=partial(calc_distance, pos=pos))))


def merge_overlapping_exons(exons, min_dist=10):
    collapsed = {}
    for e1, e2 in combinations(exons, 2):
        e1, e2 = sorted([e1, e2])
        if (e2[0] - e1[1]) < min_dist:
            new_exon = (e1[0], e2[1])
            collapsed[e1] = new_exon
            collapsed[e2] = new_exon
    exons = set([collapsed.get(e, e) for e in exons])
    return(exons)
    

def parse_event_data(event_data, only_simple_events=False, min_dist=20):
    exons = [get_coords(exon) for exon in event_data['Exons coords'].split(';')]
    exons = merge_overlapping_exons(exons, min_dist=min_dist)
    junctions = [get_coords(jc) for jc in event_data['Junctions coords'].split(';')]
    psis = [float(x) for x in  event_data['E(PSI) per LSV junction'].split(';')]
    
    exon_psis = defaultdict(lambda : 0)
    normalize_psi = False
    if not only_simple_events or len(psis) <= 2:
        for (source, target), jc_psi in zip(junctions, psis):
            source_exon = get_closest_exon(source, exons)
            target_exon = get_closest_exon(target, exons)
            if source_exon == target_exon:
                normalize_psi = True
                continue
            exon_psis[(source_exon, target_exon)] += jc_psi
    if normalize_psi:
        total = np.sum([x for x in exon_psis.values()])
        for k, v in exon_psis.items():
            exon_psis[k] = v / total
    return(exon_psis)


def build_exon_graph(gene_data, only_simple_events=False):
    graph = defaultdict(dict)
    for event_data in gene_data.to_dict(orient='index').values():
        event_data = parse_event_data(event_data, only_simple_events)
        for (source, target), psi in event_data.items():
            graph[target][source] = psi
    return(graph)


def extract_exon_psi(exon, exon_graph):
    if exon not in exon_graph:
        psi = 1
    else:
        psi = sum([psi * extract_exon_psi(source_exon, exon_graph)
                   for source_exon, psi in exon_graph[exon].items()]) 
    return(psi)


def calc_exon_psis(exon_graph, chrom, strand):
    for start, end in exon_graph:
        try:
            psi = extract_exon_psi((start, end), exon_graph)
        except RecursionError:
            print('Recursion error found when calculating exon PSI in graph')
            for s, t in exon_graph.items():
                print(chrom, s, strand, t)
            break
        # to 0-indexed
        exon_id = '{}:{}-{}{}'.format(chrom, start - 1, end, strand)
        yield({'exon_id': exon_id, 'psi': psi})


def calc_majiq_exon_psi(data, only_simple_events=False):
    data['event_type'] = ['Source' if x.split('|')[0] == 's' else 'Target'
                           for x in data['LSV Type']]
    data = data[data['ES']]
    data = data[data['event_type'].eq('Source')]
    parsed = []
    for _, gene_data in data.groupby(['Gene ID', 'event_type']):
        chrom = gene_data['chr'].unique()[0]
        strand = gene_data['strand'].unique()[0]
        exon_graph = build_exon_graph(gene_data,
                                      only_simple_events=only_simple_events)
        exon_psis = calc_exon_psis(exon_graph, chrom, strand)
        parsed.extend(exon_psis)
    exon_psis = pd.DataFrame(parsed)
    return(exon_psis)
