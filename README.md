# Exon-PSI

Toolkit to calculate or extract exon-level PSIs from different programs quantifying Alternative splicing at different levels for direct comparison

Programs supported:

- [MAJIQ](https://majiq.biociphers.org/)
- [Whippet](https://github.com/timbitz/Whippet.jl)
- [SUPPA](https://github.com/comprna/SUPPA)
- [rMATS](http://rnaseq-mats.sourceforge.net/)
- [SplAdder](https://github.com/ratschlab/spladder)
- [vast-tools](https://github.com/vastgroup/vast-tools)
- [SpliceTrap](http://rulai.cshl.edu/splicetrap/)
- [Outriger](https://yeolab.github.io/outrigger/index.html)

## Installation

Create a python3 conda environment and activate it

```bash
conda create -n exon_psi python=3.7
conda activate exon_psi
```

Download the repository using git and cd into it

```bash
git clone git@bitbucket.org:cmartiga/exon_psi.git
```

Install using setuptools

```bash
cd exon_psi
python setup.py install
```

## Test
Run tests to make sure everything works

```bash
python test/tests.py
```

## Example: calculate exon-PSI from MAJIQ output

```bash
extract_exon_psi MAJIQ_OUTPUT -o OUTPUT -p majiq
```
