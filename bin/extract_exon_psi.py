import argparse
import pandas as pd
from exon_psi.functions import calc_majiq_exon_psi
from exon_psi.settings import AVAILABLE_PROGRAMS, PROG_EVENTS_REQ
from exon_psi.parse import PSIparser


def main():
    description = 'This script derives exon-level PSIs diverse programs used'
    description += 'for inferring alternative splicing at different levels and'
    description += 'with different strategies. Returns a simple table with '
    description += 'derived PSI values for exons, using 0-based coordinates as '
    description += 'identifiers'
#     description += 'processed by voila by summing all possible paths including'
#     description += 'a particular exon. Tested for MAJIQ v.2.1-c3da3ce'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
#     help_msg = 'File with MAJIQ complete table obtained with voila tsv tool'
    help_msg = 'AS program output to parse'
    input_group.add_argument('input', help=help_msg)
    
    options_group = parser.add_argument_group('Options')
    help_msg = 'Program used. Available: {}'.format(AVAILABLE_PROGRAMS)
    options_group.add_argument('-p', '--program', required=True, help=help_msg)
    
    help_msg = 'Derive PSI only from simple ES, not overlapping other events'
    options_group.add_argument('-s', '--only_simple_ES', default=False,
                               action='store_true', help=help_msg)
    help_msg = 'Use also exon body counts for when from rMATS output (False)'
    input_group.add_argument('-e', '--exon_counts', default=False,
                             action='store_true', help=help_msg)
    
    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output CSV file with exon PŜIs')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    use_exon_counts = parsed_args.exon_counts
    out_fpath = parsed_args.output
    program = parsed_args.program
    only_simple_events = parsed_args.only_simple_ES
    
    # Run
    parser = PSIparser(program=program)
    psi = parser.parse(in_fpath, use_exon_counts=use_exon_counts,
                       only_simple_events=only_simple_events)
    psi.to_csv(out_fpath, index=False)


if __name__ == '__main__':
    main()
