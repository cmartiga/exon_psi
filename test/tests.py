#!/usr/bin/env python
import unittest
from os.path import join
from subprocess import check_call

import numpy as np

from exon_psi.functions import extract_exon_psi
from exon_psi.settings import (TEST_DATA_DIR, BIN_DIR, MAJIQ, OUTRIGGER,
                               VASTTOOLS, WHIPPET, SPLICETRAP, SPLADDER,
                               RMATS, SUPPA)
from exon_psi.parse import PSIparser
from tempfile import NamedTemporaryFile


class ProcessingTests(unittest.TestCase):
    def test_calc_exon_psi(self):
        exon_graph = {'exon2': {'exon1': 0.5},
                      'exon3': {'exon1': 0.4, 'exon2': 0.1},
                      'exon4': {'exon1': 0.1, 'exon2': 0.9, 'exon3': 0.7}}
        assert(extract_exon_psi('exon1', exon_graph) == 1)
        assert(extract_exon_psi('exon2', exon_graph) == 0.5)
        assert(extract_exon_psi('exon3', exon_graph) == 0.45)
        assert(extract_exon_psi('exon4', exon_graph) == 0.865)
    
    def test_rec_error(self):
        parser = PSIparser(program=MAJIQ)
        fpath = join(TEST_DATA_DIR, 'rec_error.tsv')
        psi = parser.parse(fpath, only_simple_events=False)
        assert(np.allclose(psi['psi'], [1.000273, 0.999727, 
                                        0.971910, 0.028090]))
    
    def test_parse_majiq(self):
        parser = PSIparser(program=MAJIQ)
        fpath = join(TEST_DATA_DIR, 'majiq.tsv')
        psi = parser.parse(fpath, only_simple_events=False)
        assert(np.all(psi.columns == ['exon_id', 'psi']))
        
        parser = PSIparser(program=MAJIQ)
        fpath = join(TEST_DATA_DIR, 'majiq2.tsv')
        psi = parser.parse(fpath, only_simple_events=False)
        assert(np.all(psi.columns == ['exon_id', 'psi']))
        
    def test_parse_outrigger(self):
        parser = PSIparser(program=OUTRIGGER)    
        fpath = join(TEST_DATA_DIR, 'outrigger.csv')
        psi = parser.parse(fpath)
        assert(np.all(psi.columns == ['exon_id', 'psi']))
    
    def test_parse_vasttools(self):
        parser = PSIparser(program=VASTTOOLS)
        fpath = join(TEST_DATA_DIR, 'vasttools.tsv')
        psi = parser.parse(fpath)
        assert(np.allclose(psi['psi'].iloc[0], 1))
        assert(psi['total'].iloc[0] == 3)
        assert(np.all(psi.columns == ['exon_id', 'psi', 'total']))
        
    def test_parse_whippet(self):
        parser = PSIparser(program=WHIPPET)
        fpath = join(TEST_DATA_DIR, 'whippet.tsv.gz')
        psi = parser.parse(fpath)
        assert(psi['total'][0] == 48)
        assert(np.allclose(psi['psi'][0], 1.0))
        assert(np.all(psi.columns == ['exon_id', 'psi', 'total']))
    
    def test_parse_splicetrap(self):
        parser = PSIparser(program=SPLICETRAP)
        fpath = join(TEST_DATA_DIR, 'splicetrap.tsv')
        psi = parser.parse(fpath)
        assert(np.allclose(psi['total'][0], 118.8118))
        assert(np.allclose(psi['psi'][0], 0.999))
        assert(np.all(psi.columns == ['exon_id', 'psi', 'total']))
    
    def test_parse_spladder(self):
        parser = PSIparser(program=SPLADDER)
        fpath = join(TEST_DATA_DIR, 'spladder.tsv.gz')
        psi = parser.parse(fpath)
        assert(np.allclose(psi['total'][0], 309.9))
        assert(np.allclose(psi['psi'][0], 0.89))
        assert(np.all(psi.columns == ['exon_id', 'psi', 'total']))
        
    def test_parse_rmats(self):
        parser = PSIparser(program=RMATS)
        psi = parser.parse(TEST_DATA_DIR)
        assert(np.allclose(psi['psi.0'].iloc[67], 0.894317))
        assert(psi['total.8'].iloc[67] == 322)
        
    def test_parse_suppa(self):
        parser = PSIparser(program=SUPPA)
        fpath = join(TEST_DATA_DIR, 'suppa.csv')
        psi = parser.parse(fpath)
        assert(np.all(psi.columns == ['exon_id', 'psi']))
    
    def test_bin(self):
        in_fpath = join(TEST_DATA_DIR, 'majiq.tsv') 
        script = join(BIN_DIR, 'extract_exon_psi.py')

        with NamedTemporaryFile('w') as fhand:
            out_fpath = fhand.name        
            cmd = ['python', script, in_fpath, '-o', out_fpath, '-p', MAJIQ]
            check_call(cmd)
            
            cmd = ['python', script, TEST_DATA_DIR, '-o', out_fpath, '-p', RMATS]
            check_call(cmd)


if __name__ == '__main__':
    import sys;sys.argv = ['', 'ProcessingTests']
    unittest.main()
