#!/usr/bin/env python
from setuptools import setup, find_packages


def main():
    description = 'Calculation of exon-PSI from diverse outputs'
    setup(
        name='exon-PSI',
        version='0.1',
        description=description,
        author_email='cmarti@cnic.es',
        url='https://bitbucket.org/cmartiga/exon_psi',
        packages=find_packages(),
        include_package_data=True,
        entry_points={
            'console_scripts': [
                'extract_exon_psi = bin.extract_exon_psi:main'
            ]},
        install_requires=['pandas'],
        platforms='ALL',
        keywords=['bioinformatics', 'alternative splicing', 'RNA-Seq'],
        classifiers=[
            "Programming Language :: Python :: 3",
            'Intended Audience :: Science/Research',
            "License :: OSI Approved :: MIT License",
            "Operating System :: OS Independent",
        ],
    )
    return


if __name__ == '__main__':
    main()
